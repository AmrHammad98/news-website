﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using News_Website.Models;

namespace News_Website.Controllers
{
    public class HomeController : Controller
    {
        // db context object to access database 
        private AppDbContext _context;

        //constructor 
        public HomeController()
        {
            _context = new AppDbContext();
        }

        // dispose db context 
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult Index()
        {
            var News = _context.News.ToList();
            return View(News);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}