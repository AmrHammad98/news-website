﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Football_News_Website_Final.Models;

namespace Football_News_Website_Final.Controllers
{
    public class EventsController : Controller
    {
        // db context object to access database 
        private AppDbContext _context;

        //constructor 
        public EventsController()
        {
            _context = new AppDbContext();
        }

        // dispose db context 
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Events
        public ActionResult Index()
        {
            var events = _context.Events.ToList();
            return View(events);
        }

        // GET: Event with id (displays 1 event)
        public ActionResult Single(int id)
        {
            var evnt = _context.Events.SingleOrDefault(e => e.ID == id);
            return View(evnt);
        }

        // Events Table
        public ActionResult List()
        {
            var events = _context.Events.ToList();
            return View(events);
        }


        public ActionResult eventsForm()
        {
            return View();
        }

        // add or update events
        [HttpPost]
        public ActionResult add_or_update(Event evnt)
        {
            if (evnt.ID == 0)
            {
                _context.Events.Add(evnt);
            }
            else
            {
                // get admin from database 
                var newsInDb = _context.Events.SingleOrDefault(n => n.ID == evnt.ID);

                // update data 
                newsInDb.Title = evnt.Title;
                newsInDb.fullContent = evnt.fullContent;
                newsInDb.partialContent = evnt.partialContent;
                newsInDb.Added_Date = evnt.Added_Date;
                newsInDb.image = evnt.image;
            }

            _context.SaveChanges();
            return RedirectToAction("List", "Events");
        }

        public ActionResult deleteNews(int id)
        {
            var events_del = _context.Events.SingleOrDefault(e => e.ID == id);

            if (events_del == null)
            {
                return HttpNotFound();
            }
            else
            {
                _context.Events.Remove(events_del);
                _context.SaveChanges();
                return RedirectToAction("List", "Events");
            }

        }

        public ActionResult Edit(int id)
        {
            var events_ed = _context.Events.SingleOrDefault(n => n.ID == id);

            if (events_ed == null)
            {
                return HttpNotFound();
            }
            else
            {
                return View("eventsForm", events_ed);
            }
        }
    }
}