﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Football_News_Website_Final.Models;

namespace Football_News_Website_Final.Controllers
{
    public class NewsController : Controller
    {
        // db context object to access database 
        private AppDbContext _context;

        //constructor 
        public NewsController()
        {
            _context = new AppDbContext();
        }

        // dispose db context 
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: News shows all news 
        public ActionResult Index()
        {
            var News = _context.News.ToList();
            return View(News);
        }

        // shows single news 
        public ActionResult Single(int id)
        {
            var single = _context.News.SingleOrDefault(s => s.ID == id);

            return View(single);
        }

        public ActionResult List()
        {
            var News = _context.News.ToList();
            return View(News);
        }

        public ActionResult newsForm()
        {
            return View();
        }

        [HttpPost]
        public ActionResult add_or_update(News news)
        {
            // if admin not yet created create otherwise update
            if (news.ID == 0)
            {
                _context.News.Add(news);
            }
            else
            {
                // get admin from database 
                var newsInDb = _context.News.SingleOrDefault(n => n.ID == news.ID);

                // update data 
                newsInDb.Title = news.Title;
                newsInDb.FullContent = news.FullContent;
                newsInDb.PartialContent = news.PartialContent;
                newsInDb.AddedDate = news.AddedDate;
                newsInDb.Image = news.Image;
            }

            _context.SaveChanges();

            //to redirect to Admin's main page 
            return RedirectToAction("List", "News");
        }

        public ActionResult deleteNews(int id)
        {
            var news_del = _context.News.SingleOrDefault(n => n.ID == id);

            if (news_del == null)
            {
                return HttpNotFound();
            }
            else
            {
                _context.News.Remove(news_del);
                _context.SaveChanges();
                return RedirectToAction("List", "News");
            }

        }

        public ActionResult Edit(int id)
        {
            var news_ed = _context.News.SingleOrDefault(n => n.ID == id);

            if (news_ed == null)
            {
                return HttpNotFound();
            }
            else
            {
                return View("newsForm", news_ed);
            }
        }
    }
}