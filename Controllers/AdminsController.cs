﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Football_News_Website_Final.Models;
using Football_News_Website_Final.ViewModel;

namespace Football_News_Website_Final.Controllers
{
    public class AdminsController : Controller
    {
        // db context object to access database 
        private AppDbContext _context;

        //constructor 
        public AdminsController()
        {
            _context = new AppDbContext();
        }

        // dispose db context 
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult Index()
        {
            var admins = _context.Admins.Include(a => a.role).ToList();
            return View(admins);
        }

        public ActionResult Single(int id)
        {
            var single = _context.Admins.Include(a => a.role).SingleOrDefault(a => a.ID == id);
            if (single == null)
            {
                return HttpNotFound();
            }
            return View(single);
        }

        public ActionResult newAdmin()
        {
            var roleNames = _context.Roles.ToList();

            var viewModel = new NewAdminCustomerViewModel
            {
                Roles = roleNames
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult add_or_update(Admin admin)
        {
            // if admin not yet created create otherwise update
            if (admin.ID == 0)
            {
                _context.Admins.Add(admin);
            }
            else
            {
                // get admin from database 
                var adminInDb = _context.Admins.Include(a => a.role).SingleOrDefault(a => a.ID == admin.ID);

                // update data 
                adminInDb.Name = admin.Name;
                adminInDb.adminName = admin.adminName;
                adminInDb.Email = admin.Email;
                adminInDb.Password = admin.Password;
                adminInDb.Active = admin.Active;
                adminInDb.roleID = admin.roleID;
            }
            
            _context.SaveChanges();

            //to redirect to Admin's main page 
            return RedirectToAction("Index", "Admins");
        }

        public ActionResult Edit(int id)
        {
            var admin_ed = _context.Admins.Include(a => a.role).SingleOrDefault(a => a.ID == id);

            if (admin_ed == null)
            {
                return HttpNotFound();
            }

            var viewModel = new NewAdminCustomerViewModel
            {
                admin = admin_ed,
                Roles = _context.Roles.ToList()
            };

            return View("newAdmin", viewModel);
        }

        public ActionResult deleteAdmin(int id)
        {
            var admin_del = _context.Admins.Include(a => a.role).SingleOrDefault(a => a.ID == id);

            if (admin_del == null)
            {
                return HttpNotFound();
            }
            else
            {
                _context.Admins.Remove(admin_del);
                _context.SaveChanges();
                return RedirectToAction("Index", "Admins");
            }
        }
    }
}