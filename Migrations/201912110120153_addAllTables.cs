﻿namespace Football_News_Website_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAllTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        roleID = c.Byte(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        adminName = c.String(nullable: false, maxLength: 50),
                        Email = c.String(),
                        Password = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AdminRoles", t => t.roleID, cascadeDelete: true)
                .Index(t => t.roleID);
            
            CreateTable(
                "dbo.AdminRoles",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        abr = c.String(nullable: false, maxLength: 5),
                        details = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Image = c.String(),
                        Title = c.String(),
                        FullContent = c.String(),
                        PartialContent = c.String(nullable: false, maxLength: 300),
                        AddedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Admins", "roleID", "dbo.AdminRoles");
            DropIndex("dbo.Admins", new[] { "roleID" });
            DropTable("dbo.News");
            DropTable("dbo.AdminRoles");
            DropTable("dbo.Admins");
        }
    }
}
