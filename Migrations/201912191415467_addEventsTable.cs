﻿namespace Football_News_Website_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addEventsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        partialContent = c.String(nullable: false, maxLength: 300),
                        fullContent = c.String(nullable: false),
                        image = c.String(),
                        Added_Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Events");
        }
    }
}
