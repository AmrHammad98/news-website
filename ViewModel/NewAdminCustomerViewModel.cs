﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Football_News_Website_Final.Models;

namespace Football_News_Website_Final.ViewModel
{
    public class NewAdminCustomerViewModel
    {
        public IEnumerable<AdminRole> Roles { get; set; }
        public Admin admin { get; set; }
    }
}