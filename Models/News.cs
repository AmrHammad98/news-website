﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Football_News_Website_Final.Models
{
    public class News
    {
        // attributes 
        public int ID { get; set; }                     // primary key for DB 
        public string Image { get; set; }               // images paths 
        public string Title { get; set; }               // title of news to be an anchor in view
        public string FullContent { get; set; }         // full content of news to be shown in single page (restricted to max )

        [Required]
        [MaxLength(300)]
        public string PartialContent { get; set; }      // partial content to be shown in news page (restricted to length of 300)

        public DateTime AddedDate { get; set; }         // date and time were the news was added 
    }
}