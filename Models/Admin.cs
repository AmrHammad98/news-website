﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Football_News_Website_Final.Models
{
    public class Admin
    {
        //attributes 
        public int ID { get; set; }                 // primary key 

        [Display(Name = "Role Name")]
        public byte roleID { get; set; }           // foreing key 

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(50)]
        [Display(Name = "Admin's Name")]
        public string adminName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public bool Active { get; set; }

        // foreign key
        public AdminRole role { get; set; }
    }
}