﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Football_News_Website_Final.Models
{
    public class AppDbContext : DbContext
    {
        //Data sets (represents tables in database)
        public DbSet<News> News { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<AdminRole> Roles{ get; set; }
        public DbSet<Event> Events { get; set; }

        //constructor 
        public AppDbContext()
        {
        }
    }
}