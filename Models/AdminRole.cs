﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Football_News_Website_Final.Models
{
    public class AdminRole
    {
        //attributes 
        public byte Id { get; set; }             //primary key 

        [Required]
        [MaxLength(5)]
        public string abr { get; set; }

        [MaxLength(100)]
        public string details { get; set; }
    }
}